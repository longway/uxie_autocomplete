monk = require('monkberry')
template = require('./autocomplete.monk')


DEBOUNCE_TIME = 200 # ms
MIN_AUTOCOMPLETE_VALUE = 2

positionAround = (targetNode, sourceNode, forcedBottom, offsets) ->
  # NOTE: IOS8 COMPAT
  unless forcedBottom?
    forcedBottom = false
  unless offsets?
    offsets = { top: 0, left: 0 }

  rect = targetNode.getBoundingClientRect()
  # NOTE: COMPATIBILITY WITH IE
  scrollX = window.scrollX or window.pageXOffset
  scrollY = window.scrollY or window.pageYOffset

  left = rect.left  + scrollX
  top =  rect.top  + scrollY + ( rect.bottom - rect.top)
  bottom = rect.bottom  + scrollY
  right = rect.right  + scrollX
  if sourceNode
    sourceNode.style.top = "#{top}px"
    sourceNode.style.left = "#{left}px"
  return top: top, left: left, right: right, bottom: bottom

class Autocomplete
  _isMounted: false
  prefix: ''
  keysMap:
    enter: 13
    up: 38
    down: 40
    left: 37
    right: 39
  timeout: null
  theme: null
  options: null

  constructor: (options) ->
    # NOTE: IOS 8 SAFARI COMPATIBILIYY
    @options = options or {}

    @wrapper = @options['wrapper'] or document.body
    @component = monk.render(@options.template or template, document.body)
    role = @options['role'] or 'json_result'
    @prefix = @options.prefix if @options.prefix
    @inputNode = @wrapper.querySelector("[role=\"#{role}\"]")

    @component.nodes[0].setAttribute('style', "width:#{@width}")
    @visibleAutocompleteResult =
      city: []
    @lastRequestTimestamp = new Date().getTime()
    @theme = @options['theme'] or null

    parentRole  = @options.parent_role
    @spinnerNodes = @wrapper.querySelector("[role=\"#{parentRole}\"]").querySelector('[role="autocomplete-spinner"]').childNodes

    @resetComponent()
    @init_events()

    @options.afterInit?(@)

    @_isMounted = true

  init_events: =>
    # NOTE: FOR IE: USE "keyup" INSTEAD OF "input" e BECAUSE
    # OTHERWISE AUTOCOMPLETE DROPDOWN WILL BE SHOW ON INITIALIZE WIDGET
    # NOTE: IF YOU'LL ADD "change" EVENT IT WILL RE-RENDER SUGGESTED ITEM AFTER CLICK ON IT
    @inputNode.addEventListener "keyup", (e) =>
      if [@keysMap.enter, @keysMap.up, @keysMap.down, @keysMap.left, @keysMap.right].indexOf(e.keyCode) > -1
        return

      if @isEnoughLettersForAutocomplete()
        @debounce(@getCities, DEBOUNCE_TIME)
      else
        @component.update { city: []}

      @options.onChangeInput?( @, e.target.value )

    @inputNode.addEventListener 'focus', -> @select()

    @inputNode.addEventListener 'keydown', (e) =>
      switch e.keyCode
        when @keysMap.enter
          if @visibleAutocompleteResult.city.length > 0
            selectedItem = @selectedItem()
            @inputNode.value = selectedItem.value
            @component.update { city: []}
            e.preventDefault()

            @options.onChange?(@, selectedItem)
        when @keysMap.down
          autocomplete_length = @visibleAutocompleteResult.city.length - 1

          if @selectedAutocompleteIndex?
            if @selectedAutocompleteIndex < autocomplete_length
              @selectedAutocompleteIndex += 1
          else
            @selectedAutocompleteIndex = 1

          @updateTemplate()
        when @keysMap.up
          if @selectedAutocompleteIndex
            @selectedAutocompleteIndex -= 1 if @selectedAutocompleteIndex > 0

          @updateTemplate()

    @component.nodes[0].addEventListener "mouseover", (e) =>
      values = (x['value'] for x in @visibleAutocompleteResult.city)

      target = e.target
      if target.nodeName is 'SPAN'
        target = target.parentNode
      liNode = target.querySelector("[role=\"autocomplete_item_value\"]")

      if liNode and values.indexOf(liNode.textContent) > -1
        @selectedAutocompleteIndex = values.indexOf(liNode.textContent)
        @updateTemplate()

    @component.nodes[0].addEventListener 'click', =>
      @handleChooseCity()

    @inputNode.addEventListener 'blur', (e) =>
      if @isEnoughLettersForAutocomplete() and @visibleAutocompleteResult.city.length > 0
        @handleChooseCity()
        @options.onBlur?(@, @chosedCity())
      else
        @closeAutocomplete()
      @showRequest = false

  chosedCity: =>
    values = @visibleAutocompleteResult.city
    return values[@selectedAutocompleteIndex]

  handleChooseCity: =>
    @inputNode.value = @chosedCity().value
    @closeAutocomplete()

    @options.onChange?(@, @chosedCity())

  resetComponent: =>
    @component.update({ prefix: @prefix, city: [] })

  updateTemplate: =>
    for el in @visibleAutocompleteResult.city
      el['selectedClass'] = ""
      el['theme'] = "#{@theme}"

    selectedAutocompleteItem = @selectedItem()
    if selectedAutocompleteItem?
      selectedAutocompleteItem['selectedClass'] = " #{@prefix}autocomplete-list-item-info--active"
      if @theme
        selectedAutocompleteItem.selectedClass += " #{@prefix}autocomplete-list-item-info--active-theme--#{@theme}"

    @component.update(city: @visibleAutocompleteResult.city, prefix: @prefix)

  isEnoughLettersForAutocomplete: =>
    @inputNode.value.length >= MIN_AUTOCOMPLETE_VALUE

  selectedItem: =>
    @selectedAutocompleteIndex ||= 0
    if (@visibleAutocompleteResult.city.length - 1) < @selectedAutocompleteIndex
      @visibleAutocompleteResult.city[0]
    else
      @visibleAutocompleteResult.city[@selectedAutocompleteIndex]


  getCities: =>
    @showRequest = true
    @selectedAutocompleteIndex = null
    @visibleAutocompleteResult.city = []

    startTimestamp = (new Date).getTime()
    @startSpinnerAnimation() if @isEnoughLettersForAutocomplete()

    @options.autocompleteClient.get
      term: @inputNode.value
      callbackName: @options.autocompleteClientCallbackName
      callback: ({ data }) =>
        # NOTE: GET CITIES ONLY IN ALLOWED LIST
        @visibleAutocompleteResult =
          city: []

        if startTimestamp > @lastRequestTimestamp and @showRequest
          inputValueRegex = new RegExp("^#{@inputNode.value}$", 'i')
          isMatchFound = false
          data = data.map (obj) =>
            if isMatchFound
              obj.isMatched = false
            else
              obj.isMatched = inputValueRegex.test( obj.value )
              if obj.isMatched
                isMatchFound = true
            return obj

          for city in data
            @visibleAutocompleteResult.city.push city
            if @visibleAutocompleteResult.city.length > 4
              break

          pos = positionAround(@inputNode, @component.nodes[0], true, { top: 2, left: 0 })
          wrapperPos = positionAround(@wrapper, false, true, { top: 2, left: 0 })

          width = wrapperPos.right - wrapperPos.left - (pos.left - wrapperPos.left) - (@options.paddingRight or 0)
          # NOTE: PECHALNO NO PRISHLOS FIXIT TAK
          @component.nodes[0].setAttribute('style',"top:#{pos.top + ( @options.offsetTop or 0 ) }px; left:#{pos.left + ( @options.offsetLeft or 0 ) }px; width: #{width}px")

          @stopSpinnerAnimation()
          @updateTemplate()
          @lastRequestTimestamp = startTimestamp

  debounce: (callback, threshold) ->
    # NOTE: IOS 8 SAFARI COMPATIBILIYY
    unless threshold?
      threshold = 100

    clearTimeout(@timeout) if @timeout

    return unless @_isMounted

    @timeout = setTimeout =>
      callback.apply(@)
      @timeout = null
    , threshold

  closeAutocomplete: ->
    @component.update { city: []}
    @inputNode.blur()

  startSpinnerAnimation: ->
    @spinner_animation_started = true
    spin.classList.remove("#{@prefix}spin--shown") for spin in @spinnerNodes
    @wrapper.classList.add("#{@prefix}widget--loading")
    setTimeout (=> @animateSpinner(@spinnerNodes[0])), 160
    setTimeout (=> @animateSpinner(@spinnerNodes[1])), 320
    setTimeout (=> @animateSpinner(@spinnerNodes[2])), 480


  animateSpinner: (node) ->
    node.classList.add("#{@prefix}spin--shown")
    setTimeout (=> node.classList.remove("#{@prefix}spin--shown")), 400
    setTimeout (=> @animateSpinner(node)), 1000 if @spinner_animation_started

  stopSpinnerAnimation: ->
    @wrapper.classList.remove("#{@prefix}widget--loading")
    @spinner_animation_started = false


module.exports = Autocomplete
